# Toy microscope models (gifts, give-aways etc.)

<img src="allthreemodels.jpg" width=100% />

from left to right: 

1) An Axio Observer looking microscope that once was a Ti2.
2) with a LSM 800 scanhead 
3) with cut outs so one can fit the original LSM800 scan mirrors (after cutting them down a bit).
With the scanner it looks like this:

<img src="withScanner.jpg" width=70% />

Of course one can print it with glow-in-the-dark filament converting it into the often requested fluorescent microscope: 

<img src="printwithglowinthedark.jpg" width=70% />
<img src="itglows.jpg" width=70% />

